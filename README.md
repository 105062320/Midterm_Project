# Software Studio 2018 Spring Midterm Project
## Notice
* **作品網址:**https://midterm-project-46b01.firebaseapp.com/
* **報告網址:**https://gitlab.com/105062320/Midterm_Project/blob/master/README.md

## Topic
* [**My Forum論壇**]
* Key functions (add/delete)
    1. [**post list**]文章列表
    2. [**post page**]點擊文章的go to postpage,就會進入每個文章的頁面
    3. [**user page**]使用者頁面,顯示使用者資料與其的文章
    4. [**leave comment under any post**]可在文章下方留言
* Other functions (add/delete)
    1. [**delete comment**]可以於文章下方刪除自己的comment(其他user無法刪除你的comment)
    2. [**favorite**]可在自己喜歡的文章加favorite,類似按讚功能
    3. [**search function**]搜尋功能,能打入文字搜尋文章,有以title搜尋以及以作者搜尋兩種模式
    4. [**comment count,favorite count**]在每個文章的右上方都有紀錄它的評論數以及favorite數
    5. [**trending post list熱門文章**]紀錄全部論壇文章各自的favorite數,並將favorite數多的排前面
    6. [**user's top post**]紀錄全部使用者本身文章的favorite數,並將favorite數多的排前面
    7. [**top button**]滑動頁面時,右下方會出現top button,點擊後會返回最上方
    8. [**music player**]於導覽欄上方有css美化的音樂播放器
    9. [**upload file,url path**]在新增post時,可以上傳file,一上傳下方就會顯示url path以及預覽button,可以點擊查看
    10. [**delete post**]可以刪除自己的文章(其他user無法刪除你的post)
    11. [**xxx**]

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|Other functions|1~10%|Y|

## Website Detail Description
* Basic Components
    1. **Membership Mechanism:**
       可以create new account以及sign in with gmail跟密碼
    2. **GitLab Page:** 
       有push到gitlab上,但好像page不是很穩定,有附firebase連結(希望使用)
    3. **Database:**
       在database.rule裡有針對登入後的使用者才給予write,read權利,雖然一先開始就會載入signin page
    4. **RWD:**
       使用@media,如果畫面小於一定程度會進行位置調整以及功能是否display的判斷
    5. **Topic Key Function:**
       於上方已有說明
* Advanced Components
    1. **Third-Party Sign In:**
       可以進行google的pop up sign in
    2. **Chrome Notification:**
       當新增一篇文章時,會跳出通知告知有新文章以及其title
    3. **Use CSS Animation:**
       在header處有小圖示會進行來回的滑動,點擊post時會以fade in方式顯示
    4. **Security Report:**
       請見下方Security Report
    5. **Other functions:**
       於上面已有詳細說明
* Website 大致架構與補充
    * 一開始會先跳出登入頁面，若用google登入，不僅會存入使用者名稱與信箱，還會記住其google的頭像作為po文以及user page頭像。
    * 登入後，網站中會有一行導覽欄(Navigation Bar)，可以點選其中選項查看trending post,post list,user post以及搜尋post等等。
    * 新增post則在導覽欄右方，若畫面小於一定程度會移至左下方，add post時可以填入title,body以及使用firebase storage來上傳附件。
    * 每個post右上方都有記錄其favorite count與comment count，標題旁的垃圾桶圖示是刪除文章的按鍵，點擊"go to post page"即可進入文章的post page。
    * 若是自己點擊文章的favorite，favorite圖像會由空心變為實心，以此紀錄自己是否收藏。

## Security Report (Optional)
1. 為了防止跨網站指令碼(Cross-site scripting)，在新增post與comment時，是以.InnerText讀取input資料，
而不是直接使用.InnerHTML。
2. 為了安全性，因此block掉使用者以F12查看網頁的功能

## References
* w3-school css (https://www.w3schools.com/w3css/4/w3.css)
* google notification
* firebase quickstart (https://github.com/firebase/quickstart-js)
* Material design lite (https://getmdl.io/)