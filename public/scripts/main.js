'use strict';
//const FIREBASE_AUTH = firebase.auth();
//const FIREBASE_MESSAGING = firebase.messaging();
//const FIREBASE_DATABASE = firebase.database();
// Shortcuts to DOM Elements.
var messageForm = document.getElementById('message-form');
var messageInput = document.getElementById('new-post-message');
var titleInput = document.getElementById('new-post-title');
var signInButton = document.getElementById('sign-in-button');
var signOutButton = document.getElementById('sign-out-button');
var splashPage = document.getElementById('page-splash');
var addPost = document.getElementById('add-post');
var addButton = document.getElementById('add');
//section

var trendingPostsSection = document.getElementById('trending-posts-list');
var recentPostsSection = document.getElementById('recent-posts-list');
var userPostsSection = document.getElementById('user-posts-list');
var topUserPostsSection = document.getElementById('top-user-posts-list');
var searchPostsSection = document.getElementById('search-posts-list');
var PostpageSection = document.getElementById('post-page-list');
//button
var trendingMenuButton = document.getElementById('menu-trending');
var recentMenuButton = document.getElementById('menu-recent');
var searchMenuButton = document.getElementById('menu-search');
var myPostsMenuButton = document.getElementById('menu-my-posts');
var myTopPostsMenuButton = document.getElementById('menu-my-top-posts');
var submit_button = document.getElementById('submit_post');

var listeningFirebaseRefs = [];

/**
 * Saves a new post to the Firebase DB.
 */
// [START write_fan_out]
function writeNewPost(uid, username, picture, title, body,url_path) {
  // A post entry.
  var postData = {
    author: username,
    uid: uid,
    body: body,
    title: title,
    starCount: 0,
    commentCount:0,
    authorPic: picture,
    url_path:url_path
  };

  // Get a key for a new Post.
  var newPostKey = firebase.database().ref().child('posts').push().key;

  // Write the new post's data simultaneously in the posts list and the user's post list.
  var updates = {};
  updates['/posts/' + newPostKey] = postData;
  updates['/user-posts/' + uid + '/' + newPostKey] = postData;

  return firebase.database().ref().update(updates);
}
// [END write_fan_out]

/**
 * Star/unstar post.
 */
// [START post_stars_transaction]
function toggleStar(postRef, uid) {
  postRef.transaction(function(post) {
    if (post) {
      if (post.stars && post.stars[uid]) {
        post.starCount--;
        post.stars[uid] = null;
      } else {
        post.starCount++;
        if (!post.stars) {
          post.stars = {};
        }
        post.stars[uid] = true;
      }
    }
    return post;
  });
}
// [END post_stars_transaction]

/**
 * Creates a post element.
 */

function createPostElement(postId, title, text, author, authorId, authorPic,url_path) {
  var uid = firebase.auth().currentUser.uid;

  var html =
      '<div  class="post post-' + postId + ' mdl-cell mdl-cell--12-col ' +
                  'mdl-cell--12-col-tablet mdl-cell--12-col-desktop mdl-grid mdl-grid--no-spacing w3-animate-opacity" >' +
        '<div class="mdl-card mdl-shadow--2dp">' +
          '<div class="mdl-card__title mdl-color--purple-500 mdl-color-text--white">' +
            '<h4 class="mdl-card__title-text" ></h4>' +
            '<span class="remove_p">'+
            '<div class="remove_post material-icons" style="color:red;cursor: pointer;">delete_forever</div>'+
            '</span>'+
            //postpage
            '<buttom class="post_page">Go to post page</buttom>'+
            
            
            //'<buttom class="remove">Remove</buttom>'+
           
          '</div>' +
          '<div class="header">' +
            '<div>' +
              '<div class="avatar"></div>' +
              //'<div class="mdl-color-text--black" style="display: inline-block;">Author:</div>'+
              '<u class="username mdl-color-text--black" style="display: inline-block;"></u>' +
              
            '</div>' +
          '</div>' +
          '<span class="star" style="display: inline-block;">' +
            
            '<div class="comment_hidden" style="display: inline-block;">'+         
              '<div class="commentCount material-icons" style="color: DeepSkyBlue;">forum</div>' +
              '<div class="comment-count">0</div>' +
            '</div>'+
            '<div class="not-starred material-icons" style="display: inline-block;" >favorite_border</div>' +
            '<div class="starred material-icons" style="display: inline-block;">favorite</div>' +
            '<div class="star-count" style="display: inline-block;">0</div>' +
            //
            
          '</span>' +
          //'<div style="font-size: 18px;">url_path:</div>' +
          '<a href="" class="url" style="font-size:16px;"></a>' +
          '<div class="text"style="font-size: 18px;"></div>' +
          
          '<div class="comments-container" id="comments-container" ></div>' +
          '<form class="add-comment" action="#">' +
            '<div class="mdl-textfield mdl-js-textfield">' +
              '<input class="mdl-textfield__input new-comment" type="text">' +
              '<label class="mdl-textfield__label">Comment...</label>' +
            '</div>' +
            
          '</form>' +
          '</div>' +
      '</div>';
  
  // Create the DOM element from the HTML.
  var div = document.createElement('div');
  div.innerHTML = html;
  var postElement = div.firstChild;
  if (componentHandler) {
    componentHandler.upgradeElements(postElement.getElementsByClassName('mdl-textfield')[0]);
  }
        //onclick notification

 
  

  var addCommentForm = postElement.getElementsByClassName('add-comment')[0];
  var commentInput = postElement.getElementsByClassName('new-comment')[0];
  var star = postElement.getElementsByClassName('starred')[0];
  var unStar = postElement.getElementsByClassName('not-starred')[0];

  // Set values.
  postElement.getElementsByClassName('url')[0].href=url_path;
  if(url_path!=""){
    postElement.getElementsByClassName('url')[0].innerText="附件";
  }
  
  //postElement.getElementsByClassName('url')[0].innerText = url_path;
  postElement.getElementsByClassName('text')[0].innerText = text;
  //postElement.getElementsByClassName('text')[0].style.display="none";
  postElement.getElementsByClassName('mdl-card__title-text')[0].innerText = title;
  postElement.getElementsByClassName('username')[0].innerText = author || /*'Anonymous'*/firebase.auth().currentUser.email;;
  postElement.getElementsByClassName('avatar')[0].style.backgroundImage = 'url("' +
      (authorPic || './silhouette.jpg') + '")';
  //var comments_container = document.getElementById('comments-container');

  // Listen for comments.
  // [START child_event_listener_recycler]
  var commentsRef = firebase.database().ref('post-comments/' + postId);
  commentsRef.on('child_added', function(data) {
    addCommentElement(postElement, data.key, data.val().text, data.val().author,postId);
    updateCommentCount(postElement, 1);
  });

  commentsRef.on('child_changed', function(data) {
    setCommentValues(postElement, data.key, data.val().text, data.val().author);
    updateCommentCount(postElement, 1);
  });

  commentsRef.on('child_removed', function(data) {
    deleteComment(postElement, data.key);
    updateCommentCount(postElement, -1);
  });
  
  // [END child_event_listener_recycler]

  // Listen for likes counts.
  // [START post_value_event_listener]
  var starCountRef = firebase.database().ref('posts/' + postId + '/starCount');
  starCountRef.on('value', function(snapshot) {
    updateStarCount(postElement, snapshot.val());
  });
  // [END post_value_event_listener]

  // Listen for the starred status.
  var starredStatusRef = firebase.database().ref('posts/' + postId + '/stars/' + uid);
  starredStatusRef.on('value', function(snapshot) {
    updateStarredByCurrentUser(postElement, snapshot.val());
  });

  // Keep track of all Firebase reference on which we are listening.
  listeningFirebaseRefs.push(commentsRef);
  listeningFirebaseRefs.push(starCountRef);
  listeningFirebaseRefs.push(starredStatusRef);


  // Create new comment.
  addCommentForm.onsubmit = function(e) {
    e.preventDefault();
    createNewComment(postId, firebase.auth().currentUser.displayName, uid, commentInput.value);
    commentInput.value = '';
    commentInput.parentElement.MaterialTextfield.boundUpdateClassesHandler();
  };

  // Bind starring action.
  var onStarClicked = function() {
    var globalPostRef = firebase.database().ref('/posts/' + postId);
    var userPostRef = firebase.database().ref('/user-posts/' + authorId + '/' + postId);
    toggleStar(globalPostRef, uid);
    toggleStar(userPostRef, uid);
  };
  unStar.onclick = onStarClicked;
  star.onclick = onStarClicked;
  //onclick post page
  //var comments_container = postElement.getElementById('comments-container');
  
  
  var post_page = postElement.getElementsByClassName('post_page')[0];
  post_page.onclick=function(){
      //postElement.getElementsByClassName('text')[0].style.display="none";
      //var comments_container = document.getElementById('comments-container');
      //comments_container.style.display = "block";
      PostpageSection.getElementsByClassName('posts-container')[0].innerHTML = '';
      //alert(comments_container);
      //hidden comment
      //alert("show");
      //comments_container.classList.remove('hidden');

      var userpag = document.getElementById('custom-alert');
      userpag.innerHTML = "";
      Startpostpage();
      showSection(PostpageSection);
      //alert("onclick!");
  }

  var removePost = postElement.getElementsByClassName('remove_post')[0];
  if(firebase.auth().currentUser.uid!=authorId){
    removePost.style.display="none";
  }else{
    removePost.style.display="inline-block";
  }  
  removePost.onclick=function(){
    firebase.database().ref('posts/' + postId).remove();
    firebase.database().ref('/user-posts/' + authorId + '/' + postId).remove();
    firebase.database().ref('/post-comments/' + authorId + '/' + postId).remove();
    updates['/posts/' + postId] = null;
    updates['/user-posts/' + authorId + '/' + postId] = null;
    firebase.database().update(updates);
  }
  //postpage--------------------------------------------------------------------------------------------------
  function Startpostpage(){
    //alert();
    //var postpageRef=firebase.database.ref('posts').child(postId).orderByChild('title').equalTo(title);
    var postpageRef=firebase.database().ref('posts').orderByChild('title').equalTo(title);
    //var postpageRef = firebase.database().ref('user-posts/' + postId);
    //var searchPostsRef = firebase.database().ref('posts').orderByChild('title').equalTo("WTF");
    var fetchPosts = function(postsRef, sectionElement) {
      postsRef.on('child_added', function(data) {
        var author = data.val().author || /*'Anonymous'*/firebase.auth().currentUser.email;;
        var containerElement = sectionElement.getElementsByClassName('posts-container')[0];
        containerElement.insertBefore(
          createPostElement(data.key, data.val().title, data.val().body, author, data.val().uid, data.val().authorPic,data.val().url_path),
          containerElement.firstChild);
      });
      postsRef.on('child_changed', function(data) {
        var containerElement = sectionElement.getElementsByClassName('posts-container')[0];
        var postElement = containerElement.getElementsByClassName('post-' + data.key)[0];
        postElement.getElementsByClassName('mdl-card__title-text')[0].innerText = data.val().title;
        postElement.getElementsByClassName('username')[0].innerText = data.val().author;
        postElement.getElementsByClassName('text')[0].innerText = data.val().body;
        postElement.getElementsByClassName('star-count')[0].innerText = data.val().starCount;
      });
      postsRef.on('child_removed', function(data) {
        var containerElement = sectionElement.getElementsByClassName('posts-container')[0];
        var post = containerElement.getElementsByClassName('post-' + data.key)[0];
        post.parentElement.removeChild(post);
      });
    };
  
    // Fetching and displaying all posts of each sections.
    fetchPosts(postpageRef, PostpageSection);
    // Keep track of all Firebase refs we are listening to.
    listeningFirebaseRefs.push(postpageRef);
  };  

  function showSection(sectionElement) {
    trendingPostsSection.style.display = 'none';
    recentPostsSection.style.display = 'none';
    userPostsSection.style.display = 'none';
    topUserPostsSection.style.display = 'none';
    addPost.style.display = 'none';
    searchPostsSection.style.display = 'none';
    PostpageSection.style.display = 'none';
  
    trendingMenuButton.classList.remove('is-active');
    recentMenuButton.classList.remove('is-active');
    myPostsMenuButton.classList.remove('is-active');
    myTopPostsMenuButton.classList.remove('is-active');
    searchMenuButton.classList.remove('is-active');
  
    if (sectionElement) {
      sectionElement.style.display = 'block';
    }
  }


  return postElement;
}

/**
 * Writes a new comment for the given post.
 * 
 * 
 */
function createNewComment(postId, username, uid, text) {
  firebase.database().ref('post-comments/' + postId).push({
    text: text,
    author: username,
    uid: uid
  });
}

/**
 * Updates the starred status of the post.
 */
function updateStarredByCurrentUser(postElement, starred) {
  if (starred) {
    postElement.getElementsByClassName('starred')[0].style.display = 'inline-block';
    postElement.getElementsByClassName('not-starred')[0].style.display = 'none';
  } else {
    postElement.getElementsByClassName('starred')[0].style.display = 'none';
    postElement.getElementsByClassName('not-starred')[0].style.display = 'inline-block';
  }
}

/**
 * Updates the number of stars displayed for a post.
 */
function updateStarCount(postElement, nbStart) {
  postElement.getElementsByClassName('star-count')[0].innerText = nbStart;
}

function updateCommentCount(postElement, nbComment) {
  var tmp=postElement.getElementsByClassName('comment-count')[0].innerText ;
  var count = parseInt(tmp, 10)+nbComment;
  postElement.getElementsByClassName('comment-count')[0].innerText =count ;

}
/**
 * Creates a comment element and adds it to the given postElement.
 */
function addCommentElement(postElement, id, text, author,postId) {
  var comment = document.createElement('div');
  comment.classList.add('comment-' + id);
  if(firebase.auth().currentUser.displayName==author){
    comment.innerHTML = '<span class="username"></span><span class="comment"><div> </div></span><div class="remove_com material-icons" style="color:red;">close</div>';
  }else {
    comment.innerHTML = '<span class="username"></span><span class="comment"><div> </div></span><div class="remove_com material-icons" style="color:red;display:none;">close</div>';
  }
  
  comment.getElementsByClassName('comment')[0].innerText = text;
  comment.getElementsByClassName('username')[0].innerText = author || /*'Anonymous'*/firebase.auth().currentUser.email;;

  var commentsContainer = postElement.getElementsByClassName('comments-container')[0];
  commentsContainer.appendChild(comment);
  comment.getElementsByClassName('remove_com')[0].onclick=function(){firebase.database().ref('post-comments/' + postId).child(id).remove();}
}

/**
 * Sets the comment's values in the given postElement.
 */

function setCommentValues(postElement, id, text, author) {
  var comment = postElement.getElementsByClassName('comment-' + id)[0];
  comment.getElementsByClassName('comment')[0].innerText = text;
  comment.getElementsByClassName('fp-username')[0].innerText = author;
}

/**
 * Deletes the comment of the given ID in the given postElement.
 */

function deleteComment(postElement, id) {
  var comment = postElement.getElementsByClassName('comment-' + id)[0];
  comment.parentElement.removeChild(comment);
}



/**
 * Starts listening for new posts and populates posts lists.
 */
function startDatabaseQueries(search_txt) {
  // [START my_top_posts_query]
  var myUserId = firebase.auth().currentUser.uid;
  var trendingPostsRef = firebase.database().ref('posts').orderByChild('starCount')/*.limitToLast(5)*/;

  var topUserPostsRef = firebase.database().ref('user-posts/' + myUserId).orderByChild('starCount')/*.limitToLast(5)*/;
  // [END my_top_posts_query]
  // [START recent_posts_query]
  var recentPostsRef = firebase.database().ref('posts').limitToLast(100);
  // [END recent_posts_query]
  var userPostsRef = firebase.database().ref('user-posts/' + myUserId);
  var searchPostsRef="";
  var postpageRef="";
  //var searchPostsRef = firebase.database().ref('posts').orderByChild('title').equalTo("WTF");

  var fetchPosts = function(postsRef, sectionElement) {
    postsRef.on('child_added', function(data) {
      var author = data.val().author || /*'Anonymous'*/firebase.auth().currentUser.email;;
      var containerElement = sectionElement.getElementsByClassName('posts-container')[0];
      containerElement.insertBefore(
        createPostElement(data.key, data.val().title, data.val().body, author, data.val().uid, data.val().authorPic,data.val().url_path),
        containerElement.firstChild);
    });
    postsRef.on('child_changed', function(data) {
      var containerElement = sectionElement.getElementsByClassName('posts-container')[0];
      var postElement = containerElement.getElementsByClassName('post-' + data.key)[0];
      postElement.getElementsByClassName('mdl-card__title-text')[0].innerText = data.val().title;
      postElement.getElementsByClassName('username')[0].innerText = data.val().author;
      postElement.getElementsByClassName('text')[0].innerText = data.val().body;
      postElement.getElementsByClassName('star-count')[0].innerText = data.val().starCount;
    });
    postsRef.on('child_removed', function(data) {
      var containerElement = sectionElement.getElementsByClassName('posts-container')[0];
      var post = containerElement.getElementsByClassName('post-' + data.key)[0];
      post.parentElement.removeChild(post);
    });
  };

  // Fetching and displaying all posts of each sections.
  fetchPosts(trendingPostsRef, trendingPostsSection);
  fetchPosts(topUserPostsRef, topUserPostsSection);
  fetchPosts(recentPostsRef, recentPostsSection);
  fetchPosts(userPostsRef, userPostsSection);
  fetchPosts(searchPostsRef, searchPostsSection);
  fetchPosts(postpageRef, PostpageSection);
  // Keep track of all Firebase refs we are listening to.
  listeningFirebaseRefs.push(trendingPostsRef);
  listeningFirebaseRefs.push(topUserPostsRef);
  listeningFirebaseRefs.push(recentPostsRef);
  listeningFirebaseRefs.push(userPostsRef);
  listeningFirebaseRefs.push(searchPostsRef);
  listeningFirebaseRefs.push(postpageRef);
}

/**
 * Writes the user's data to the database.
 */
// [START basic_write]
function writeUserData(userId, name, email, imageUrl) {
  firebase.database().ref('users/' + userId).set({
    username: name,
    email: email,
    profile_picture : imageUrl
  });
}
// [END basic_write]

/**
 * Cleanups the UI and removes all Firebase listeners.
 */
function cleanupUi() {
  // Remove all previously displayed posts.
  trendingPostsSection.getElementsByClassName('posts-container')[0].innerHTML = '';
  topUserPostsSection.getElementsByClassName('posts-container')[0].innerHTML = '';
  recentPostsSection.getElementsByClassName('posts-container')[0].innerHTML = '';
  userPostsSection.getElementsByClassName('posts-container')[0].innerHTML = '';
  searchPostsSection.getElementsByClassName('posts-container')[0].innerHTML = '';
  PostpageSection.getElementsByClassName('posts-container')[0].innerHTML = '';

  // Stop all currently listening Firebase listeners.
  listeningFirebaseRefs.forEach(function(ref) {
    ref.off();
  });
  listeningFirebaseRefs = [];
}

/**
 * The ID of the currently signed-in User. We keep track of this to detect Auth state change events that are just
 * programmatic token refresh but not a User status change.
 */
var currentUID;

/**
 * Triggers every time there is a change in the Firebase auth state (i.e. user signed-in or user signed out).
 */
function onAuthStateChanged(user) {
  // We ignore token refresh events.
  if (user && currentUID === user.uid) {
    return;
  }

  cleanupUi();
  if (user) {
    currentUID = user.uid;
    splashPage.style.display = 'none';
    writeUserData(user.uid, user.displayName, user.email, user.photoURL);
    startDatabaseQueries();
  } else {
    // Set currentUID to null.
    currentUID = null;
    // Display the splash page where you can sign-in.
    splashPage.style.display = '';
  }
}

/**
 * Creates a new post for the current user.
 */
function newPostForCurrentUser(title, text,url_path) {
  // [START single_value_read]
  var userId = firebase.auth().currentUser.uid;
  return firebase.database().ref('/users/' + userId).once('value').then(function(snapshot) {
    var username = (snapshot.val() && snapshot.val().username) || /*'Anonymous'*/firebase.auth().currentUser.email;;
    // [START_EXCLUDE]
    return writeNewPost(firebase.auth().currentUser.uid, username,
      firebase.auth().currentUser.photoURL,
      title, text,url_path);
    // [END_EXCLUDE]
  });
  // [END single_value_read]
}

/**
 * Displays the given section element and changes styling of the given button.
 */
function showSection(sectionElement, buttonElement) {
  trendingPostsSection.style.display = 'none';
  recentPostsSection.style.display = 'none';
  userPostsSection.style.display = 'none';
  topUserPostsSection.style.display = 'none';
  addPost.style.display = 'none';
  searchPostsSection.style.display = 'none';
  PostpageSection.style.display = 'none';

  trendingMenuButton.classList.remove('is-active');
  recentMenuButton.classList.remove('is-active');
  myPostsMenuButton.classList.remove('is-active');
  myTopPostsMenuButton.classList.remove('is-active');
  searchMenuButton.classList.remove('is-active');

  if (sectionElement) {
    sectionElement.style.display = 'block';
  }
  if (buttonElement) {
    buttonElement.classList.add('is-active');
  }
}

// Bindings on load.
window.addEventListener('load', function() {
    //test
    var txtName = document.getElementById('inputName');
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnSignUp = document.getElementById('btnSignUp');
    btnLogin.addEventListener('click', function () {
      /// TODO 2: Add email login button event
      ///         1. Get user input email and password to login
      ///         2. Back to index.html when login success
      ///         3. Show error message by "create_alert" and clean input field
        //var name = txtName.value;
        var email = txtEmail.value;
        var password = txtPassword.value;

        firebase.auth().signInWithEmailAndPassword(email, password).then(function (firebaseUser) {
            //create_alert("success", "");
        }).catch(function (error) {
            //create_alert("error", error.code, error.message);
            txtEmail.value = "";
            txtPassword.value = "";
        });
        //var s = document.getElementById("myAudio"); 
        //s.play(); 
    });
  
    btnGoogle.addEventListener('click', function () {
        /// TODO 3: Add google login button event
        ///         1. Use popup function to login google
        ///         2. Back to index.html when login success
        ///         3. Show error message by "create_alert"
  
        var provider = new firebase.auth.GoogleAuthProvider();
  
        firebase.auth().signInWithPopup(provider).then(function (result) {
            var token = result.credential.accessToken;
            var user = result.user;
           // create_alert("success", "");
        }).catch(function (error) {
           // create_alert("error", error.code, error.message);
            txtEmail.value = "";
            txtPassword.value = "";
        });

        
        //var s = document.getElementById("myAudio"); 
        //s.play(); 
    });
  
    btnSignUp.addEventListener('click', function () { 
      /// TODO 4: Add signup button event
      ///         1. Get user input email and password to signup
      ///         2. Show success message by "create_alert" and clean input field
      ///         3. Show error message by "create_alert" and clean input field
  
        var email = txtEmail.value;
        var password = txtPassword.value;
        firebase.auth().createUserWithEmailAndPassword(email, password).then(function (firebaseUser) {
            //create_alert("success", "");
        }).catch(function (error) {
            //create_alert("error", error.code, error.message);
            txtEmail.value = "";
            txtPassword.value = "";
        });
        //var s = document.getElementById("myAudio"); 
        //s.play(); 
    });
  //--------------------------------------------------------------------
  // Bind Sign in button.
  signInButton.addEventListener('click', function() {
    var provider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().signInWithPopup(provider);
    //var s = document.getElementById("myAudio"); 
    //s.play(); 
  });

  // Bind Sign out button.
  signOutButton.addEventListener('click', function() {
    var s = document.getElementById("myAudio"); 
    s.pause(); 
    firebase.auth().signOut();
  });

  // Listen for auth state changes
  firebase.auth().onAuthStateChanged(onAuthStateChanged);
  
  // Saves message on form submit.
  messageForm.onsubmit = function(e) {
    var url = document.getElementById('url');
    e.preventDefault();
    var url_path=url.value;
    var text = messageInput.value;
    var title = titleInput.value;
    var photo = firebase.auth().currentUser.photoURL;
    //submit_button.onclick=function(){
    if (Notification.permission == "granted"){
              //alert("suceed");
              //postId, title, text, author, authorId, authorPic
        
        var notification = new Notification('A New Post was added!', {
                //icon: authorPic,
                icon:photo||'./silhouette.jpg',
                body: "title: "+title,
        });
        notification.onclick = function () {
                window.open("https://forum-test-8c55f.firebaseapp.com/");      
        };
    }
            
    //} 
    if (text && title) {
      newPostForCurrentUser(title, text,url_path).then(function() {
        myPostsMenuButton.click();
      });

      url.value = '';
      messageInput.value = '';
      titleInput.value = '';
    }
  };
  //search--------------------------------------------------------
  searchMenuButton.onclick = function() {
    var text= document.getElementById('search_text');
    var alertarea = document.getElementById('custom-alert');
    alertarea.innerHTML = "";
    searchPostsSection.getElementsByClassName('posts-container')[0].innerHTML = '';
    var option = document.getElementById("mySelect").value;
    Startsearch(text.value,option);
    //alert(text.value);
    //alertarea.innerHTML = "";
    //firebase.database().ref('notifications').child(key).remove();
    //searchMenuButton.classList.remove('is-active');
    showSection(searchPostsSection, searchMenuButton);
  };
  function Startsearch(search_text,option){
    var searchPostsRef ="";
    searchPostsRef = firebase.database().ref('posts').orderByChild(option).equalTo(search_text);

  var fetchPosts = function(postsRef, sectionElement) {
    postsRef.on('child_added', function(data) {
      var author = data.val().author || /*'Anonymous'*/firebase.auth().currentUser.email;;
      var containerElement = sectionElement.getElementsByClassName('posts-container')[0];
      containerElement.insertBefore(
        createPostElement(data.key, data.val().title, data.val().body, author, data.val().uid, data.val().authorPic,data.val().url_path),
        containerElement.firstChild);
    });
    postsRef.on('child_changed', function(data) {
      var containerElement = sectionElement.getElementsByClassName('posts-container')[0];
      var postElement = containerElement.getElementsByClassName('post-' + data.key)[0];
      postElement.getElementsByClassName('mdl-card__title-text')[0].innerText = data.val().title;
      postElement.getElementsByClassName('username')[0].innerText = data.val().author;
      postElement.getElementsByClassName('text')[0].innerText = data.val().body;
      postElement.getElementsByClassName('star-count')[0].innerText = data.val().starCount;
    });
    postsRef.on('child_removed', function(data) {
      var containerElement = sectionElement.getElementsByClassName('posts-container')[0];
      var post = containerElement.getElementsByClassName('post-' + data.key)[0];
      post.parentElement.removeChild(post);
    });
  };



  // Fetching and displaying all posts of each sections.
  fetchPosts(searchPostsRef, searchPostsSection);
  // Keep track of all Firebase refs we are listening to.
  listeningFirebaseRefs.push(searchPostsRef);
  };
  // Bind menu buttons.
  recentMenuButton.onclick = function() {
    var alertarea = document.getElementById('custom-alert');
    alertarea.innerHTML = "";
    showSection(recentPostsSection, recentMenuButton);
  };
  myPostsMenuButton.onclick = function() {
    
    var email=firebase.auth().currentUser.email;
    var user=firebase.auth().currentUser.displayName||"Joe";
    /*if(user==""){
      user="Joe";
    }*/
    var alertarea = document.getElementById('custom-alert');
    var str_html = 
    "<div class='alert alert-primary' role='alert' style='font-size:20px;line-height: 28px;text-align: center;margin:0;'>"+
    "<strong class='alert-heading'style='font-size:35px;line-height: 38px; font-family: 'Teko', sans-serif;'>User Page </strong>"+
    "</div>"+
    "<div>"+
    "<div class='alert alert-light  ' role='alert' style='font-size:20px;line-height: 28px;text-align: center;margin:0;background-color:rgba(240, 248, 255, 0.774);'>"+
    //"<img src="+firebase.auth().currentUser.photoURL+"></img>" +
    '<div class="user_img" ></div>' +
    "<br>"+
    "<strong style='font-size:25px;'>User:</strong>" + user +
    "<br>"+
    "<strong style='font-size:25px;'>E-mail:</strong>" + email +
    //"<hr>"+
    //"<p class='mb-0'> </p>"+
    "</div>"+
    "<div class='alert alert-success' role='alert' style='font-size:20px;line-height: 28px;text-align: center;margin:0;'>"+
    "<strong style='font-size:25px;'>User's Post:</strong>" +
    "</div>";//
    
    
    //"<div class='alert alert-primary alert-dismissible fade show' role='alert'><strong>使用者名稱:</strong>" + user + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
    alertarea.innerHTML = str_html;
    
    //alert("使用者名稱: "+user+"\n"+"顯示使用者PO文紀錄");
   
    showSection(userPostsSection, myPostsMenuButton);//./silhouette.jpg
    //var pic = document.getElementsByClassName('user_img')[0];
    //var profile_picture='./silhouette.jpg'||'firebase.auth().currentUser.photoURL';
    if(user=="Joe"){
      document.getElementsByClassName('user_img')[0].style.backgroundImage = "url('./silhouette.jpg')";
    }else{
    //var photo=firebase.database().ref().child('users'+uid).child('profile_picture')
    /*firebase.database().ref.child('users').orderByChild('profile_picture').equalTo('https://lh6.googleusercontent.com/-DsAgmaNtA2U/AAAAAAAAAAI/AAAAAAAALHU/mUbmJtPd4eQ/photo.jpg').on("value", function(snapshot) {
      console.log(snapshot.val());
      snapshot.forEach(function(data) {
          console.log(data.key);
      });
    });*/
    document.getElementsByClassName('user_img')[0].style.backgroundImage = "url("+firebase.auth().currentUser.photoURL+")";
    }
  };

  trendingMenuButton.onclick = function() {
    var alertarea = document.getElementById('custom-alert');
    var str_html ="";
  
    //"<div class='alert alert-primary alert-dismissible fade show' role='alert'><strong>使用者名稱:</strong>" + user + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
    alertarea.innerHTML = str_html;
    showSection(trendingPostsSection, trendingMenuButton);
  };

  myTopPostsMenuButton.onclick = function() {
    var email=firebase.auth().currentUser.email;
    var user=firebase.auth().currentUser.displayName||"Joe";
    var alertarea = document.getElementById('custom-alert');
    var str_html = 
    "<div class='alert alert-primary' role='alert' style='font-size:20px;line-height: 28px;text-align: center;margin:0;'>"+
    "<strong class='alert-heading'style='font-size:35px;line-height: 38px; font-family: 'Teko', sans-serif;'>User Page </strong>"+
    "</div>"+
    "<div class='alert alert-light  ' role='alert' style='font-size:20px;line-height: 28px;text-align: center;margin:0;background-color:rgba(240, 248, 255, 0.774);'>"+
    //'<img src="firebase.auth().currentUser.photoURL" ></img>' +
    '<div class="user_img" ></div>' +
    "<br>"+
    "<strong style='font-size:25px;'>User:</strong>" + user +
    "<br>"+
    "<strong style='font-size:25px;'>E-mail:</strong>" + email +
    //"<hr>"+
    //"<p class='mb-0'> </p>"+
    "</div>"+
    "<div class='alert alert-danger' role='alert' style='font-size:20px;line-height: 28px;text-align: center;margin:0;'>"+
    "<strong style='font-size:25px;'>User's Post:<div style='font-size:20px;display:inline-block'>(Sort by favorite)</div></strong>" +
    "</div>";
    
    //"<div class='alert alert-primary alert-dismissible fade show' role='alert'><strong>使用者名稱:</strong>" + user + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
    alertarea.innerHTML = str_html;
    showSection(topUserPostsSection, myTopPostsMenuButton);
    if(user=="Joe"){
      document.getElementsByClassName('user_img')[0].style.backgroundImage = "url('./silhouette.jpg')";
    }else{
    document.getElementsByClassName('user_img')[0].style.backgroundImage = "url('"+firebase.auth().currentUser.photoURL+"')";
    }
  };
  addButton.onclick = function() {
    var alertarea = document.getElementById('custom-alert');
    var file = document.getElementById('file');
    alertarea.innerHTML = "";
    var Url = document.getElementById("url");
    document.getElementById('linkbox').innerHTML = "";
    Url.innerHTML =  "" ;
    showSection(addPost);
    file.value="";
    messageInput.value = '';
    //url_path.value = '';
    titleInput.value = '';
  };
  recentMenuButton.onclick();

 
}, false);

//storage----------------------------------------------------------------------
    var auth = firebase.auth();
    var storageRef = firebase.storage().ref();

    function handleFileSelect(evt) {
      evt.stopPropagation();
      evt.preventDefault();
      var file = evt.target.files[0];

      var metadata = {
        'contentType': file.type
      };

      // Push to child path.
      // [START oncomplete]
      storageRef.child('images/' + file.name).put(file, metadata).then(function(snapshot) {
        console.log('Uploaded', snapshot.totalBytes, 'bytes.');
        console.log('File metadata:', snapshot.metadata);
        // Let's get a download URL for the file.
        snapshot.ref.getDownloadURL().then(function(url) {
          console.log('File available at', url);
          // [START_EXCLUDE]
          document.getElementById('linkbox').innerHTML = '<a href="' +  url + '" style="font-size:18px;">Click to Preview</a>';
          
          var Url = document.getElementById("url");
          Url.innerHTML =   url ;
          console.log(Url.innerHTML)
          Url.select();
          document.execCommand("copy");
          //document.getElementById('link_new').innerHTML = <div>url</div>;
          // [END_EXCLUDE]
        });
      }).catch(function(error) {
        // [START onfailure]
        console.error('Upload failed:', error);
        // [END onfailure]
      });
      // [END oncomplete]
    }

    window.onload = function() {
      document.getElementById('file').addEventListener('change', handleFileSelect, false);
      document.getElementById('file').disabled = true;

      auth.onAuthStateChanged(function(user) {
        if (user) {
          //console.log('Anonymous user signed-in.', user);
          document.getElementById('file').disabled = false;
        } else {
          //console.log('There was no anonymous session. Creating a new anonymous user.');
          // Sign the user in anonymously since accessing Storage requires the user to be authorized.
          auth.signInAnonymously().catch(function(error) {
            if (error.code === 'auth/operation-not-allowed') {

            }
          });
        }
      });
    }
//top buttom-----------------------------------------------------
window.onscroll = function() {scrollFunction()};

function scrollFunction() {
  var elmnt = document.getElementById("myDIV");
  var y = elmnt.scrollTop;
  //console.log(elmnt.scrollTop );
    if ( elmnt.scrollTop> 20 || elmnt.scrollTop > 20) {
        document.getElementById("myBtn").style.display = "block";
    } else {
        document.getElementById("myBtn").style.display = "none";
    }
}

// When the user clicks on the button, scroll to the top of the document
function topFunction() {
    //alert("top!");
    var elmnt = document.getElementById("myDIV");
    //console.log("elmnt.scrollTop:"+elmnt.scrollTop );
    //document.body.scrollTop = document.documentElement.scrollTop = 0;
    elmnt.scrollTop = 0; // For Chrome, Firefox, IE and Opera
    
}

document.addEventListener('DOMContentLoaded', function () {
  if (!Notification) {
      alert('Desktop notifications not available in your browser. Try Chromium.'); 
      return;
  }else{
    //alert('Notification'); 
  } 
  if (Notification.permission !== "granted"){
      Notification.requestPermission();
      //alert('Desktop1'); 
  }/*else {
    alert('Desktop '); 
    var notification = new Notification('Notification', {
        icon: 'img/icon.png',
        body: "A message have been send to Arcadia",
    });
    notification.onclick = function () {
        window.open("https://forum-test-8c55f.firebaseapp.com/");      
    };
  }*/
});
