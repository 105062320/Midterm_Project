# Software Studio 2018 Spring Midterm Project
## Notice
**Firebase網址:**https://midterm-project-46b01.firebaseapp.com/

## Topic
* [**My Forum論壇**]
* Key functions (add/delete)
    1. [**post list**]文章列表
    2. [**post page**]點擊文章的go to postpage,就會進入每個文章的頁面
    3. [**user page**]使用者頁面,顯示使用者資料與其的文章
    4. [**leave comment under any post**]可在文章下方留言
* Other functions (add/delete)
    1. [**delete comment**]可以於文章下方刪除自己的comment(其他user無法刪除你的comment)
    2. [**favorite**]可在自己喜歡的文章加favorite,類似按讚功能
    3. [**search function**]搜尋功能,能打入文字搜尋文章,有以title搜尋以及以作者搜尋兩種模式
    4. [**comment count,favorite count**]在每個文章的右上方都有紀錄它的評論數以及favorite數
    5. [**trending post list熱門文章**]紀錄全部論壇文章各自的favorite數,並將favorite數多的排前面
    6. [**user's top post**]紀錄全部使用者本身文章的favorite數,並將favorite數多的排前面
    7. [**top button**]滑動頁面時,右下方會出現top button,點擊後會返回最上方
    8. [**music player**]於導覽欄上方有css美化的音樂播放器
    9. [**upload file,url path**]在新增post時,可以上傳file,一上傳下方就會顯示url path以及預覽button,可以點擊查看,submit之後會出現"附件"連結至上傳的檔案
    10. [**delete post**]可以刪除自己的文章(其他user無法刪除你的post)
    11. [**xxx**]

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|Other functions|1~10%|Y|

## Website Detail Description
* Basic Components
    1. **Membership Mechanism:**
       可以create new account以及sign in with gmail跟密碼
    2. **GitLab Page:** 
       有push到gitlab上,但好像page不是很穩定,有附firebase連結(希望使用)
    3. **Database:**
       在database.rule裡有針對登入後的使用者才給予write,read權利,雖然因為一開始就會載入signin page
    4. **RWD:**
       使用@media,如果畫面小於一定程度會進行位置調整以及功能是否display的判斷
    5. **Topic Key Function:**
       於上方已有說明
* Advanced Components
    1. **Third-Party Sign In:**
       可以進行google的pop up sign in
    2. **Chrome Notification:**
       當新增一篇文章時,會跳出通知告知有新文章以及其title
    3. **Use CSS Animation:**
       在header處有小圖示會進行來回的滑動
    4. **Security Report:**
       請見下方
    5. **Other functions:**
       於上面已有說明

## Security Report (Optional)
1. 為了防止跨網站指令碼(Cross-site scripting)，在新增post與comment時，是以.InnerText讀取input資料，
而不是直接使用.InnerHTML。
2. 為了安全性，因此block掉使用者以F12查看網頁的功能